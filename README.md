# javascripts to build swisscovery deeplinks

## new_acquisitions
creating deeplinks for acquisitions (based on local IZ-field lds45 [Fachcodes, DDC] and lds50 for NEL-Code)

examples: 
* Fachcode: javascript:fnel('et') -> https://basel.swisscovery.org/discovery/search?query=lds50,contains,NELA1002105%20OR%20NELA1252105%20OR%20NELA1302105%20OR%20NELA1402105%20OR%20NELA1702105&query=lds45,contains,et&tab=UBS&search_scope=UBS&vid=41SLSP_UBS:live&lang=de&mode=advanced&offset=0
* library-code: javascript:fnel('a206')-> https://basel.swisscovery.org/discovery/search?query=lds50,contains,NELA2062105&tab=UBS&search_scope=UBS&vid=41SLSP_UBS:live&lang=de&mode=advanced&offset=0

further documentation within javascript file swisscovery-nel.js

## search_iframes 
iFrame to integrate within CMS to post search within swisscovery

### search_iframes/such_iframe_small_swisscovery*.html
german and english version for small search form without any search restrictions

example: https://ub-webcontent.ub.unibas.ch/cat_search/such_iframe_small_swisscovery.html

Integrated: https://ub.unibas.ch/de/

### such_iframe_small_flex.html
search restrictions can be applied to search form
* q_label= label for search field and button
* q_field = which swisscovery index shall be searched
* b_field= preset field for which a filter shall be applied
* b_query= filter value for preset field (b_field)

Example: https://ub-webcontent.ub.unibas.ch/cat_search/such_iframe_small_swisscovery_flex.html?q_label=Personen%20/%20Orte%20Basler%20Bibliographie&b_field=any&b_query=basb*&q_field=lds43

Integrated: https://ub.unibas.ch/de/ub-als-kantonsbibliothek/klassifikation-basler-bibliographie/

### such_iframe_small_flex_ebooks.html
same as such_iframe_small_flex.html, but object type (facet=rtype,include,books) 
is set to books and to online resources (&facet=tlevel,include,online_resources)

Example: https://ub-webcontent.ub.unibas.ch/cat_search/such_iframe_small_flex_ebooks.html?q_field=any&q_label=EBooks

Integrated: https://ub.unibas.ch/de/find-e-book/

### such_iframe_small_oafinder
Adaptation of swissbib search iframes for [oafinder.](https://finder.open-access.network/)

such_iframe_small_oafinder_de.html - version for German site

such_iframe_small_oafinder_en.html - version for English site

/*
  swisscovery-nel.js
  JavaScript fuer Neuerwerbungslistenabfrage aus TYPO3
  17.02.2014 / abi
  23.03.2017 / bmt: Anpassung fÃžr DDC
  23.03.2017 / bmt: Anpassung Video aufgrund neuem Formatfilter in swissbib
  17.12.2020 / mre: Anpassung für swisscovery: Zusammenfassen von DDC und Fachcodes in einem swisscovery Index: lds45
  01.04.2020 / mre: aufgeräumt
  25.06.2021 / mre: lds45 ist nun lds24

  Wichtiger Hinweis: fnel erwartet beliebig viele Bibliothekscodes (vierstellig), beliebig viele Fachcodes (zweistellig) und beliebig viele DDC-Notationen (drei- oder fünfstellig).
  Diese werden beim Aufruf hintereinandergeschrieben (Reihenfolge beliebig) und mit + getrennt.
  z.B. A138+ae oder is+or+tu oder A100+bb+090+090.4
  Hinweis: Die Unterscheidung zwischen Fachcodes und DDC bzgl. Länge ist nun überflüssig, da im gleichen Index lds24 gesucht wird
  Werden keine Bibliotheken angegeben, wird automatisch A100+A125+A130+A140+A170 angenommen.
  Sonderfunktion: video (ist separat programmiert).
*/

function fnel( arg ) {
	var arg=decodeURI(arg)
	var baseurl ="https://basel.swisscovery.org/discovery/search?query=";
	var fachcodes = new Array();
	var libraries = new Array();
	var ddc = new Array();
	var ccl = "";

	// Aufsplitten der Argumente aufgrund ihrer Laenge

	switch( arg ) {
		case "video":
			ccl = "lds50,contains,NELA100" + make_date();
			ccl += "%20OR%20NELA125" + make_date();
			ccl += "%20OR%20NELA130" + make_date();
			ccl += "%20OR%20NELA140" + make_date();
			ccl += "%20OR%20NELA170" + make_date();
			ccl += "&pfilter=rtype,exact,videos,AND";
			break;
		default:
			var argsplit = arg.split("+");
			for (var i = 0; i < argsplit.length; i++) {
				if ( argsplit[i].length == 4 ) {
					libraries.push(argsplit[i]);
					continue;
				} else if ( argsplit[i].length == 2 ) {
					fachcodes.push(argsplit[i]);
					continue;
				} else if ( argsplit[i].length == 3 ) {
					ddc.push(argsplit[i]);
					continue;
				} else if ( argsplit[i].length == 5 ) {
					ddc.push(argsplit[i]);
					continue;
				} else {
					alert("Vermutlich falscher Parameter:\n" + argsplit[i]);
					return;
				}
			}

			if ( libraries.length == 0 ) {
				libraries.push("A100","A125","A130","A140", "A170");
			}

			if ( libraries.length == 1 && fachcodes.length == 0 && ddc.length == 0 ) {
				ccl = "lds50,contains," + make_nel(libraries);
			}
			else {
					all_codes = [].concat(fachcodes, ddc);
    				ccl = "lds50,contains," + make_nel(libraries) + make_wfc_ddc(all_codes) ;
			}
			break;
	}
  // alert( "CCL-Abfrage:\n" + ccl );
  myurl = baseurl + ccl + "&tab=UBS&search_scope=UBS&vid=41SLSP_UBS:live&lang=de&mode=advanced&offset=0";
  window.open(myurl,"_blank");
}

// ============= Diverse Hilfsfunktionen

// Hilfsfunktion fuer Datum

function make_date() {
	// construct date prev month as "yymm"
	var now=new Date();
	var date;
	var year=now.getYear();
	if ( year < 1900) year+=1900;
	var month=now.getMonth();
	if ( month == 0 ) {
		month=12;
		year--;
	}
	year -= 2000;
	if ( year < 10 ) {
		date = "0" + year;
	}
	if ( year == 0 ) {
		date = "00";
	}
	else {
		date = year;
	}
	if ( month < 10 ) { date += "0" + month; }
// Achtung: "" ist noetig, damit year und month nicht summiert werden !
	else { date += "" + month; }

	return date;
}

// Hilfsfunktionen fuer komplexe Anbfragen

// Abfragen nach Fachcode (wfc)

function make_wfc_ddc(searcharray) {
	var cclterm = "&query=lds24,contains,";

	for (var i = 0; i < searcharray.length; i++) {
		cclterm += "" + encodeURI(searcharray[i]);
		if (i != searcharray.length -1) {
		    cclterm +="%20OR%20"
        }
	}

	return cclterm;
}

// Abfragen nach NELA-Codes

function make_nel(searcharray) {
	var cclterm = "";
	for (var i = 0; i < searcharray.length; i++) {
		cclterm += "NEL" + encodeURI(searcharray[i].toUpperCase()) + make_date();
		if (i != searcharray.length -1) {
		    cclterm +="%20OR%20"
        }
	}

	return cclterm;
}

// Abfragen nach DDC (an zweiter Position): obsolet, da DDC und Fachcode im Feld lds24

function make_ddc(searcharray) {
    var cclterm = "&query=lds24,contains,";

    for (var i = 0; i < searcharray.length; i++) {
        cclterm += "" + encodeURI(searcharray[i]);
        if (i != searcharray.length -1) {
		    cclterm +="*%20OR%20"
        }
    }

    return cclterm;
}

// Abfragen nach DDC (an dritter Position): obsolet, da DDC und Fachcode im Feld lds24

function make_ddc_2(searcharray) {
    var cclterm = "&query=lds24,contains,";

    for (var i = 0; i < searcharray.length; i++) {
        cclterm += "" + encodeURI(searcharray[i]);
        if (i != searcharray.length -1) {
		    cclterm +="*%20OR%20"
        }
    }

    return cclterm;
}

